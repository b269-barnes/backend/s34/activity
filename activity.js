const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended : true}));

// GET
app.get("/home", (request, response) => {
	response.send("Welcome to the page")
});


// GET USERS
app.get("/users", (request,response) => {

	response.send ([{
		"username" : "johndoe",
		"password" : "johndoe1234"
	}])

});

// DELETE

app.delete("/delete-user", (request, response) => {
    const { username } = request.body;
    let deletedUser;

    if (username) {
    
        deletedUser = `User ${username} has been deleted.`;
    } else {
     
        deletedUser = "No username provided."
    }
    response.send(deletedUser);
})

	

app.listen(port, () => console.log(`Server running at ${port}`));